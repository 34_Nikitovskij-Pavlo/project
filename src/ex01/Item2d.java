package ex01;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Хранит исходные данные и результат вычислений.
 *
 * @author Pavlo Nikitovskij
 * @version 1.0
 */
public class Item2d implements Serializable {
    /**
     * Аргумент вычисляемой функции.
     */
// transient
    private int decimal;
    /**
     * Результат вычисления функции.
     */
    private int[] help = new int[10];
    /**
     * Автоматически сгенерированная константа
     */
    private static final long serialVersionUID = 1L;

    /**
     * Инициализирует поля {@linkplain Item2d#decimal}, {@linkplain Item2d#help}
     */
    public Item2d() {
        decimal = 0;
        help = null;
    }

    /**
     * Устанавливает значения полей: аргумента
     * и результата вычисления функции.
     *
     * @param decimal - значение для инициализации поля {@linkplain Item2d#decimal}
     * @param help    - значение для инициализации поля {@linkplain Item2d#help}
     */
    public Item2d(int decimal, int[] help) {
        this.decimal = decimal;
        this.help = help;
    }

    /**
     * Установка значения поля {@linkplain Item2d#decimal}
     *
     * @param decimal - значение для {@linkplain Item2d#decimal}
     * @return Значение {@linkplain Item2d#decimal}
     */
    public int setDecimal(int decimal) {
        return this.decimal = decimal;
    }

    /**
     * Получение значения поля {@linkplain Item2d#decimal}
     *
     * @return Значение {@linkplain Item2d#decimal}
     */
    public int getDecimal() {
        return decimal;
    }

    /**
     * Установка значения поля {@linkplain Item2d#help}
     *
     * @param help - значение для {@linkplain Item2d#help}
     * @return Значение {@linkplain Item2d#help}
     */
    public int[] setHelp(int[] help) {
        return this.help = help;
    }

    /**
     * Получение значения поля {@linkplain Item2d#help}
     *
     * @return значение {@linkplain Item2d#help}
     */
    public int[] getHelp() {
        return help;
    }

    /**
     * Установка значений {@linkplain Item2d#decimal} и {@linkplain Item2d#help}
     *
     * @param decimal - значение для {@linkplain Item2d#decimal}
     * @param help    - значение для {@linkplain Item2d#help}
     * @return this
     */
    public Item2d setHelpDecimal(int decimal, int[] help) {
        this.decimal = decimal;
        this.help = help;
        return this;
    }

    /**
     * Представляет результат вычислений в виде строки.<br>{@inheritDoc}
     */
    @Override
    public String toString() {
        return "Decimal = " + decimal + ", Array = " + Arrays.toString(help);
    }

    /**
     * Автоматически сгенерированный метод.<br>{@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Item2d other = (Item2d) obj;
        if (Double.doubleToLongBits(decimal) != Double.doubleToLongBits(other.decimal))
            return false;
// изменено сравнение результата вычисления функции
        if (!Arrays.equals(help, other.help)) {
            return false;
        }
        return true;
    }
}
