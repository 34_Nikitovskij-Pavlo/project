package ex01;

import org.junit.Test;

import java.io.IOException;

import static junit.framework.Assert.fail;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * Выполняет тестирование разработанных классов.
 *
 * @author Pavlo Nikitovskij
 * @version 1.0
 */
public class MainTest {
    /**
     * Проверка основной функциональности класса {@linkplain Calc}
     */
    @Test
    public void testCalc() {
        Calc calc = new Calc();
        calc.init(0);
        assertArrayEquals(new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, calc.getResult().getHelp());
        calc.init(100);
        assertArrayEquals(new int[]{2, 1, 0, 0, 0, 0, 0, 0, 0, 0}, calc.getResult().getHelp());
        calc.init(200);
        assertArrayEquals(new int[]{2, 0, 1, 0, 0, 0, 0, 0, 0, 0}, calc.getResult().getHelp());
        calc.init(300);
        assertArrayEquals(new int[]{2, 0, 0, 1, 0, 0, 0, 0, 0, 0}, calc.getResult().getHelp());
        calc.init(400);
        assertArrayEquals(new int[]{2, 0, 0, 0, 1, 0, 0, 0, 0, 0}, calc.getResult().getHelp());
    }

    /**
     * Проверка сериализации. Корректность восстановления данных.
     */
    @Test
    public void testRestore() {
        Calc calc = new Calc();
        int decimal;
        int[] help;
        for (int ctr = 0; ctr < 1000; ctr++) {
            decimal = (int) (Math.random() * 10000);
            help = calc.init(decimal);
            try {
                calc.save();
            } catch (IOException e) {
                fail(e.getMessage());
            }
            calc.init((int) (Math.random() * 10000));
            try {
                calc.restore();
            } catch (Exception e) {
                fail(e.getMessage());
            }
            assertArrayEquals(help, calc.getResult().getHelp());
            assertEquals(decimal, calc.getResult().getDecimal());
        }
    }
}