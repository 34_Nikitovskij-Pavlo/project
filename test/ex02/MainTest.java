package ex02;

import ex01.Item2d;
import junit.framework.Assert;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
/** Выполняет тестирование
 * разработанных классов.
 * @author Pavlo Nikitovskij
 * @version 2.0
 */
public class MainTest {
    /** Проверка основной функциональности класса {@linkplain ViewResult} */
    @Test
    public void testCalc() {
        ViewResult view = new ViewResult(5);
        view.init(100);
        Item2d item = new Item2d();
        int ctr = 0;
        item.setHelpDecimal(0, new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0});
        assertTrue("expected:<" + item + "> but was:<" + view.getItems().get(ctr) + ">",
                view.getItems().get(ctr).equals(item));
        ctr++;
        item.setHelpDecimal(100, new int[]{2, 1, 0, 0, 0, 0, 0, 0, 0, 0});
        assertTrue("expected:<" + item + "> but was:<" + view.getItems().get(ctr) + ">",
                view.getItems().get(ctr).equals(item));
        ctr++;
        item.setHelpDecimal(200, new int[]{2, 0, 1, 0, 0, 0, 0, 0, 0, 0});
        assertTrue("expected:<" + item + "> but was:<" + view.getItems().get(ctr) + ">",
                view.getItems().get(ctr).equals(item));
        ctr++;
        item.setHelpDecimal(300, new int[]{2, 0, 0, 1, 0, 0, 0, 0, 0, 0});
        assertTrue("expected:<" + item + "> but was:<" + view.getItems().get(ctr) + ">",
                view.getItems().get(ctr).equals(item));
        ctr++;
        item.setHelpDecimal(400, new int[]{2, 0, 0, 0, 1, 0, 0, 0, 0, 0});
        assertTrue("expected:<" + item + "> but was:<" + view.getItems().get(ctr) + ">",
                view.getItems().get(ctr).equals(item));
    }
    /** Проверка сериализации. Корректность восстановления данных. */
    @Test
    public void testRestore() {
        ViewResult view1 = new ViewResult(1000);
        ViewResult view2 = new ViewResult();
// Вычислим значение функции со случайным шагом приращения аргумента
        view1.init((int) (Math.random() * 1000));
// Сохраним коллекцию view1.items
        try {
            view1.viewSave();
        } catch (IOException e) {
            Assert.fail(e.getMessage());
        }
// Загрузим коллекцию view2.items
        try {
            view2.viewRestore();
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
// Должны загрузить столько же элементов, сколько сохранили
        assertEquals(view1.getItems().size(), view2.getItems().size());
// Причем эти элементы должны быть равны.
// Для этого нужно определить метод equals
        assertTrue("containsAll()", view1.getItems().containsAll(view2.getItems()));
    }
}