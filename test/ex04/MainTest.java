package ex04;

import ex01.Calc;
import ex01.Item2d;
import ex02.ViewResult;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * Тестирование класса
 * ChangeItemCommand
 *
 * @author xone
 * @version 4.0
 * @see ChangeItemCommand
 */
public class MainTest {
    /**
     * Проверка метода {@linkplain ChangeItemCommand#execute()}
     */
    @Test
    public void testExecute() {
        ChangeItemCommand cmd = new ChangeItemCommand();
        cmd.setItem(new Item2d());
        int decimal;
        int[] help = new int[10];
        int offset;
        Calc calc = new Calc();
        for (int ctr = 0; ctr < 1000; ctr++) {
            for(int i = 0; i < help.length; i++) {
                help[i] *= (int)(Math.random() * 1000);
            }
            help = cmd.getItem().setHelp(help);

            cmd.getItem().setHelpDecimal(decimal = (int) (Math.random() * 1000), help);
            cmd.setOffset(offset = (int)(Math.random() * 1000));
            cmd.execute();
            assertEquals(decimal, cmd.getItem().getDecimal());

            help = cmd.getItem().getHelp();
            for(int i = 0; i < help.length; i++) {
                help[i] *= offset;
            }
            help = cmd.getItem().setHelp(help);
            assertArrayEquals(help, cmd.getItem().getHelp());
        }
    }

    /**
     * Проверка класса {@linkplain ChangeConsoleCommand}
     */
    @Test
    public void testChangeConsoleCommand() {
        ChangeConsoleCommand cmd = new ChangeConsoleCommand(new ViewResult());
        cmd.getView().viewInit();
        cmd.execute();
        assertEquals("'c'hange", cmd.toString());
        assertEquals('c', cmd.getKey());
    }
}