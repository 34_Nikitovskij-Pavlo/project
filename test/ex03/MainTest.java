package ex03;

import ex01.Item2d;
import junit.framework.Assert;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Выполняет тестирование
 * разработанных классов.
 *
 * @author Pavlo Nikitovskij
 * @version 3.0
 */
public class MainTest {
    /**
     * Проверка основной функциональности класса {@linkplain ViewTable}
     */
    @Test
    public void testCalc() {
        ViewTable tbl = new ViewTable(10, 5);
        assertEquals(10, tbl.getWidth());
        assertEquals(5, tbl.getItems().size());
        tbl.init(40, 100);
        Item2d item = new Item2d();
        int ctr = 0;
        item.setHelpDecimal(0, new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0});
        assertTrue("expected:<" + item + "> but was:<" + tbl.getItems().get(ctr) + ">",
                tbl.getItems().get(ctr).equals(item));
        ctr++;
        item.setHelpDecimal(100, new int[]{2, 1, 0, 0, 0, 0, 0, 0, 0, 0});
        assertTrue("expected:<" + item + "> but was:<" + tbl.getItems().get(ctr) + ">",
                tbl.getItems().get(ctr).equals(item));
        ctr++;
        item.setHelpDecimal(200, new int[]{2, 0, 1, 0, 0, 0, 0, 0, 0, 0});
        assertTrue("expected:<" + item + "> but was:<" + tbl.getItems().get(ctr) + ">",
                tbl.getItems().get(ctr).equals(item));
        ctr++;
        item.setHelpDecimal(300, new int[]{2, 0, 0, 1, 0, 0, 0, 0, 0, 0});
        assertTrue("expected:<" + item + "> but was:<" + tbl.getItems().get(ctr) + ">",
                tbl.getItems().get(ctr).equals(item));
        ctr++;
        item.setHelpDecimal(400, new int[]{2, 0, 0, 0, 1, 0, 0, 0, 0, 0});
        assertTrue("expected:<" + item + "> but was:<" + tbl.getItems().get(ctr) + ">",
                tbl.getItems().get(ctr).equals(item));
    }

    /**
     * Проверка сериализации. Корректность восстановления данных.
     */
    @Test
    public void testRestore() {
        ViewTable tbl1 = new ViewTable(10, 1000);
        ViewTable tbl2 = new ViewTable();
// Вычислим значение функции со случайным шагом приращения аргумента
        tbl1.init(30, (int)(Math.random() * 1000));
// Сохраним коллекцию tbl1.items
        try {
            tbl1.viewSave();
        } catch (IOException e) {
            Assert.fail(e.getMessage());
        }
// Загрузим коллекцию tbl2.items
        try {
            tbl2.viewRestore();
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
// Должны загрузить столько же элементов, сколько сохранили
        assertEquals(tbl1.getItems().size(), tbl2.getItems().size());
// Причем эти элементы должны быть равны.
// Для этого нужно определить метод equals
        assertTrue("containsAll()", tbl1.getItems().containsAll(tbl2.getItems()));
    }
}